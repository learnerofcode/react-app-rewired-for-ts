const rewireTypescript = require('./tools/cra-rewire-typescript');

/* config-overrides.js */
module.exports = function override(config, env) {
  config = rewireTypescript(config, env);
  return config;
};
